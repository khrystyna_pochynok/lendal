package com.leoart.lendal

import android.content.Context
import data.network.UserRequest
import data.network.UsersRequestImp
import domain.staff.User
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito
import java.util.*

import org.mockito.Mockito.*;


/**
 * Created by bogdan on 6/21/16.
 */
class FriendTest {

    var context: Context? = null

    var user: User? = null
    var friend: User? = null

    var userRequest: UserRequest? = null

    @Before
    fun seTup(){
        context = Mockito.mock(Context::class.java, "context")
        user = User("test@gmail.com", "password")
        friend = User("testFriend@gmail.com", "password")
        userRequest = UsersRequestImp(context)
    }

    @Test
    @Throws(Exception::class)
    fun testFindFriend(){
        userRequest?.search("userName")
    }

    @Test
    @Throws(Exception::class)
    fun testAddFriend(){
        user?.friends = ArrayList<User>()
        if(friend!=null) {
            user?.friends?.add(friend as User)
        }

        assertEquals(1, user?.friends?.size)
    }
}