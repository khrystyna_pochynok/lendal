package domain.staff

import rx.Observable

/**
 * User staff collection represents relational paradigm for controlling staff according to user
 * Created by bogdan on 6/10/16.
 */
class UserStaffCollection : Staff<StaffEntity, User> {

    override fun addToCollection(id: StaffEntity, t: User): Observable<Boolean> {
        throw UnsupportedOperationException()
    }

    override fun addToBookMarks(id: StaffEntity, t: User): Observable<Boolean> {
        throw UnsupportedOperationException()
    }

    override fun addNew(id: StaffEntity, t: User): Observable<Boolean> {
        throw UnsupportedOperationException()
    }

    override fun remove(id: StaffEntity, t: User): Observable<Boolean> {
        throw UnsupportedOperationException()
    }
}