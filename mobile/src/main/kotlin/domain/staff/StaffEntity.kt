package domain.staff

import java.util.*

/**
 * Created by bogdan on 6/10/16.
 */
class StaffEntity {
    var name: String = ""
    var created: Date? = null

    constructor(name: String, created: Date?) {
        this.name = name
        this.created = created
    }

}