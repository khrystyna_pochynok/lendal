package domain.staff

/**
 * Created by bogdan on 6/10/16.
 */
class User {
    var id: Long? = null
    var email: String? = null
    var password: String? = null
    var staffs: MutableList<StaffEntity>? = null
    var friends: MutableList<User>? = null

    constructor(email: String?, password: String?) {
        this.email = email
        this.password = password
    }
}