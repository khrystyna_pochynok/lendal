package domain.staff

import rx.Observable

/**
 * Staff sharing interface
 *
 * Created by bogdan on 6/7/16.
 */
interface Staff<G, T> {

    /**
     * add staff G To T
     */
    fun addToCollection(id: G, t: T): Observable<Boolean>

    /**
     * saves G into bookmarks of T
     */
    fun addToBookMarks(id: G, t: T): Observable<Boolean>

    /**
     * add new G into T
     */
    fun addNew(id: G, t: T): Observable<Boolean>

    /**
     * removes G from T
     */
    fun remove(id: G, t: T): Observable<Boolean>
}