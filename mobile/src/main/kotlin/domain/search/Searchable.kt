package domain.search

import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
interface Searchable <T>{
    fun search(searchPhrase: String): Observable<T>
}