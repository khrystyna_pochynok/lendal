package domain.auth

import data.models.UserEntity

/**
 * Created by bogdan on 6/10/16.
 */
interface AuthListener {
    fun onSuccess(user: UserEntity)
    fun onError(message: String)
}