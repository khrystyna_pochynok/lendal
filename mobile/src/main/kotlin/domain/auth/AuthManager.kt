package domain.auth

import android.text.TextUtils

/**
 * Created by bogdan on 6/10/16.
 */
class AuthManager : Auth{

    override fun makeLogin(email: String, password: String, authListener: AuthListener) {
        if(TextUtils.isEmpty(email)){
            throw UnsupportedOperationException()
        }
        if(TextUtils.isEmpty(password)) {
            throw UnsupportedOperationException()
        }

    }

    override fun register(email: String, password: String, authListener: AuthListener) {
        throw UnsupportedOperationException()
    }

}