package domain.auth

/**
 * Created by bogdan on 6/10/16.
 */
interface Auth {
    fun makeLogin(email: String, password: String, authListener: AuthListener)
    fun register(email: String, password: String, authListener: AuthListener)
}