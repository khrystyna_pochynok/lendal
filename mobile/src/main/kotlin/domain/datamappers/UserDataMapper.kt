package domain.datamappers

import data.models.UserEntity
import domain.staff.User

/**
 * Created by bogdan on 6/21/16.
 */
class UserDataMapper : DataMapper<User> {

    val userEntity: UserEntity

    constructor(userEntity: UserEntity) {
        this.userEntity = userEntity
    }

    override fun transform(): User {
        return User(userEntity.email, userEntity.password)
    }
}