package domain.datamappers

/**
 * Created by bogdan on 6/21/16.
 */
interface DataMapper<T> {
    fun transform(): T
}