package data.models

/**
 * Created by bogdan on 6/10/16.
 */
class UserEntity {
    var id: String? = null
    var userName: String? = null
    var email: String? = null
    var password: String? = null
    var staff: MutableList<Staff>? = null

    constructor(email: String?, password: String?) {
        this.email = email
        this.password = password
    }

    constructor(userName: String?, email: String?, password: String?) {
        this.userName = userName
        this.email = email
        this.password = password
    }


}