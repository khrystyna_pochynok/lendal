package data.models

import java.util.*

/**
 * Created by bogdan on 6/13/16.
 */
class Photo {
    var name: String? = null
    var path: String? = null
    var created: Date? = null
}