package data.models

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import java.util.*

/**
 * Created by bogdan on 6/10/16.
 */
@IgnoreExtraProperties
class Staff {
    var userID: String? = null
    var name: String? = null
    var status: String? = null
    var photos: List<Photo>? = null
    var createdDate: Date? = null
    var rentTillDate: Date? = null

    constructor(name: String?) {
        this.name = name
        this.status = ""
        this.createdDate = Date()
    }

    constructor(name: String?, status: String?, photos: kotlin.collections.List<Photo>) {
        this.name = name
        this.status = status
        this.photos = photos
    }

    @Exclude
    fun toMap(): Map<String, Any> {
        var result = HashMap<String, Any>();
        result.put("userId", userID ?: "");
        result.put("name", name ?: "");
        result.put("status", status ?: "");
        result.put("createdDate", createdDate.toString());
        result.put("rentTillDate", rentTillDate.toString());
        return result;
    }
}