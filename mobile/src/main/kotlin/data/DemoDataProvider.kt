package data

import domain.staff.StaffEntity
import java.util.*

/**
 * Created by bogdan on 6/10/16.
 */
class DemoDataProvider {
    fun getStaffList(): MutableList<StaffEntity>? {
        val result = ArrayList<StaffEntity>()
        result.add(StaffEntity("Test", Date()))
        return result
    }
}