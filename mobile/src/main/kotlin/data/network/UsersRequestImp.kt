package data.network

import android.content.Context
import data.models.UserEntity
import data.network.firebase.FireBaseUserRequest
import rx.Observable

/**
 * Created by bogdan on 6/22/16.
 */
class UsersRequestImp(context: Context?) : UserRequest, NetworkRequest(context) {

    override  fun addFriend(userID: String, friend: UserEntity): Observable<Boolean>{
        throw UnsupportedOperationException()
    }

    override fun search(searchPhrase: String): Observable<UserEntity>{
        return FireBaseUserRequest(context).search(searchPhrase)
    }
}