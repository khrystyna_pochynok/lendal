package data.network

import data.models.UserEntity
import domain.search.Searchable
import rx.Observable

/**
 * Created by bogdan on 6/21/16.
 */
interface UserRequest : Searchable<UserEntity> {
    fun addFriend(userID: String, friend: UserEntity): Observable<Boolean>
}