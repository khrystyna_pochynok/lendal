package data.network

import android.content.Context
import data.network.firebase.LoginFireBaseRequest
import domain.staff.User
import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
class LoginRequest(context: Context?) : Login<User>, NetworkRequest(context) {

    override fun login(email: String, password: String): Observable<User> {
        return LoginFireBaseRequest(context).makeLogin(email, password)
    }

}