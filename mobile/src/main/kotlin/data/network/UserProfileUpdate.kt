package data.network

import rx.Observable

/**
 * an interface for updating a user's basic profile information
 * Created by bogdan on 6/14/16.
 */
interface UserProfileUpdate<T>{
    fun updateEmail(emailAddress:String): Observable<Boolean>
    fun updatePassword(password: String): Observable<Boolean>
    fun updateProfile(update: T): Observable<Boolean>
}