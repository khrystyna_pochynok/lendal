package data.network

import android.content.Context
import data.network.firebase.FBUserProfileChangeRequest
import rx.Observable

/**
 * Created by bogdan on 6/14/16.
 */
class UserProfileUpdateRequest(context: Context?) : NetworkRequest(context), UserProfileUpdate<UserProfile>{

    override fun updateEmail(emailAddress: String): Observable<Boolean> {
        return FBUserProfileChangeRequest(context).updateEmail(emailAddress)
    }

    override fun updatePassword(password: String): Observable<Boolean> {
        return FBUserProfileChangeRequest(context).updatePassword(password)
    }

    override fun updateProfile(update: UserProfile): Observable<Boolean> {
        return FBUserProfileChangeRequest(context).updateProfile(update)
    }
}