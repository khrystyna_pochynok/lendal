package data.network

import data.models.Staff
import rx.Observable

/**
 * Created by bogdan on 6/14/16.
 */
interface StaffRequest<T> {

    fun getAllStaff(userID: String): Observable<T>

}