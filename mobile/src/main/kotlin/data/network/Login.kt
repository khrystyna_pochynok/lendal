package data.network

import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
interface Login<T> {
    fun login(email: String, password: String) : Observable<T>
}