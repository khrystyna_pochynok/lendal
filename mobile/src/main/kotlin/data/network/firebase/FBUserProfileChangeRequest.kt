package data.network.firebase

import android.content.Context
import android.net.Uri
import com.google.firebase.auth.UserProfileChangeRequest
import data.network.UserProfile
import data.network.UserProfileUpdate
import rx.Observable

/**
 *  FireBase request for updating a user's basic profile information
 *
 * Created by bogdan on 6/14/16.
 */
class FBUserProfileChangeRequest(context: Context?) : FireBaseAuth(context), UserProfileUpdate<UserProfile> {

    override fun updateEmail(emailAddress: String): Observable<Boolean> {
        return inTransActionBool {
            Observable.create({ subscriber ->
                mAuth?.currentUser?.updateEmail(emailAddress)
                        ?.addOnCompleteListener {
                            subscriber.onNext(it.isSuccessful)
                            subscriber.onCompleted()
                        }
            })
        }
    }

    override fun updatePassword(password: String): Observable<Boolean> {
        return inTransActionBool {
            Observable.create({ subscriber ->
                mAuth?.currentUser?.updatePassword(password)
                        ?.addOnCompleteListener {
                            subscriber.onNext(it.isSuccessful)
                            subscriber.onCompleted()
                        }
            })
        }
    }

    override fun updateProfile(update: UserProfile): Observable<Boolean> {
        return inTransActionBool {
            Observable.create({ subscriber ->
                if (update.displayName != null || update.photoUri != null) {
                    val userProfileChangeRequest = getUserProfileChangeRequest(update)
                    mAuth?.currentUser?.updateProfile(userProfileChangeRequest.build())
                            ?.addOnCompleteListener {
                                subscriber.onNext(it.isSuccessful)
                                subscriber.onCompleted()
                            }
                } else {
                    subscriber.onError(Throwable("Data is not valid"))
                    subscriber.onCompleted()
                }
            })
        }
    }

    private fun getUserProfileChangeRequest(update: UserProfile): UserProfileChangeRequest.Builder {
        val userProfileChangeRequest = UserProfileChangeRequest.Builder()

        if (update.displayName != null) {
            userProfileChangeRequest.setDisplayName(update.displayName)
        }

        if (update.photoUri != null) {
            userProfileChangeRequest.setPhotoUri(Uri.parse(update.photoUri))
        }
        return userProfileChangeRequest
    }
}