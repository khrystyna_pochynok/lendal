package data.network.firebase

import android.content.Context

import domain.staff.User
import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
class LoginFireBaseRequest(context: Context?) : FireBaseAuth(context) {

    fun makeLogin(email: String, password: String): Observable<User> {
        val result: Observable<User>
        try {
            result = Observable.create({ subscriber ->
                mAuth?.signInWithEmailAndPassword(email, password)
                        ?.addOnCompleteListener {
                            if (it.isSuccessful) {
                                subscriber.onNext(User(email, password))
                                subscriber.onCompleted()
                            } else {
                                subscriber.onError(Throwable(it.exception?.message))
                                subscriber.onCompleted()
                            }
                        }
            })
        } catch(e: Exception) {
            e.printStackTrace()
            result = Observable.error(Throwable(e.message))
        }
        return result
    }
}