package data.network.firebase

import android.content.Context
import data.models.Staff
import data.network.StaffRequest
import rx.Observable

/**
 * Created by bogdan on 6/14/16.
 */
class FireBaseStaffRequest(context: Context?) : FireBaseRequest(context), StaffRequest<Staff> {

    override  fun getAllStaff(userID: String): Observable<Staff>{
        val result : Observable<Staff>
        try{
            result = Observable.create({subscriber->
                //mDataBase.
            })
        }catch (e: Exception){
            result = Observable.error(Throwable(e.message))
        }
        return result
    }
}