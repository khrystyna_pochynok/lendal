package data.network.firebase

import android.content.Context
import data.models.UserEntity
import data.network.UserRequest
import rx.Observable

/**
 * Created by bogdan on 6/21/16.
 */
class FireBaseUserRequest(context: Context?) : FireBaseRequest(context), UserRequest {

    override fun addFriend(userID: String, friend: UserEntity): Observable<Boolean> {
        throw UnsupportedOperationException()
    }

    override fun search(searchPhrase: String): Observable<UserEntity>{
        throw UnsupportedOperationException()
    }
}