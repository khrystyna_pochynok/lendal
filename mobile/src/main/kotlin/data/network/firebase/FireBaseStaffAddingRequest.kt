package data.network.firebase

import android.content.Context
import data.models.Staff
import rx.Observable
import java.util.*

/**
 * Created by bogdan on 6/13/16.
 */
class FireBaseStaffAddingRequest(context: Context?) : FireBaseRequest(context) {

    private val STAFFS = "staffs"
    private val USERS_TAFF = "user-staffs"

    fun addStaffToUser(userID: String, staff: Staff) : Observable<Boolean>{
        var result: Observable<Boolean>
        try{
            result = Observable.create({ subscriber ->
                var key = mDataBase?.child(STAFFS)?.push()?.key;
                staff.userID = userID
                var postValues = staff.toMap();
                var childUpdates = HashMap<String, Any>();
                childUpdates.put("/$STAFFS/$key", postValues);
               // childUpdates.put("/$USERS_TAFF/$userID/$key", postValues);
                mDataBase.updateChildren(childUpdates)
                subscriber.onNext(true)
                subscriber.onCompleted()
            })
        }catch (e: Exception){
            result = Observable.error(Throwable(e.message))
        }
        return result
    }
}