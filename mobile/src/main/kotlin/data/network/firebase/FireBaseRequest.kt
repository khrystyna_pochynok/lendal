package data.network.firebase

import android.content.Context
import com.google.firebase.database.FirebaseDatabase
import data.models.UserEntity
import data.network.NetworkRequest
import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
abstract class FireBaseRequest(context: Context?) : NetworkRequest(context) {
    private val BASE_URL = "https://lendal.firebaseio.com/"
    protected val mDataBase = FirebaseDatabase.getInstance().reference

    protected fun inTransActionBool(fireBaseRequest: () -> Observable<Boolean>): Observable<Boolean> {
        val result: Observable<Boolean>
        try {
            result = fireBaseRequest()
        } catch(e: Exception) {
            e.printStackTrace()
            result = Observable.error(Throwable(e.message))
        }
        return result
    }

    protected fun inTransActionUser(fireBaseRequest: () -> Observable<UserEntity>): Observable<UserEntity> {
        val result: Observable<UserEntity>
        try {
            result = fireBaseRequest()
        } catch(e: Exception) {
            e.printStackTrace()
            result = Observable.error(Throwable(e.message))
        }
        return result
    }
}