package data.network.firebase

import android.content.Context
import com.google.firebase.auth.FirebaseAuth

/**
 * Created by bogdan on 6/13/16.
 */
abstract class FireBaseAuth : FireBaseRequest {
    protected var mAuth: FirebaseAuth? = null

    constructor(context: Context?) : super(context) {
        this.mAuth = FirebaseAuth.getInstance()
    }
}