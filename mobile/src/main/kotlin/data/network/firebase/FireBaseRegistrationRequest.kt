package data.network.firebase

import android.content.Context
import data.models.UserEntity
import rx.Observable
import java.util.*

/**
 * Created by bogdan on 6/10/16.
 */
class FireBaseRegistrationRequest(context: Context?) : FireBaseAuth(context) {

    fun register(email: String, password: String): Observable<UserEntity> {
        return inTransActionUser {
             Observable.create({ subscriber ->
                mAuth?.createUserWithEmailAndPassword(email, password)
                        ?.addOnCompleteListener {
                            if (it.isSuccessful) {
                                createUser(mAuth?.currentUser?.uid ?: UUID.randomUUID().toString(), email, password)
                                subscriber?.onNext(UserEntity(email, password))
                                subscriber?.onCompleted()
                            } else {
                                subscriber?.onError(Throwable(it.exception?.message ?: "some error occurred"))
                                subscriber?.onCompleted()
                            }
                        }
            })
        }
    }

    fun createUser(userId: String, email: String, password: String): Unit{
        val user = UserEntity(email, password)
        mDataBase?.child("users")?.child(userId)?.setValue(user)
    }
}