package data.network

import android.content.Context

/**
 * Created by bogdan on 6/10/16.
 */
abstract class NetworkRequest {

    var context: Context? = null

    constructor(context: Context?) {
        this.context = context
    }
}