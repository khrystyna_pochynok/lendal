package data.network

import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
interface Registration<T> {
    fun register(email: String, password: String): Observable<T>
}