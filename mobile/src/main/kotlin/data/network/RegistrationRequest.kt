package data.network

import android.content.Context
import data.models.UserEntity
import data.network.firebase.FireBaseRegistrationRequest
import rx.Observable

/**
 * Created by bogdan on 6/10/16.
 */
class RegistrationRequest(context: Context?) : Registration<UserEntity>, NetworkRequest(context) {

    override fun register(email: String, password: String): Observable<UserEntity> {
        return FireBaseRegistrationRequest(context).register(email, password)
    }

}