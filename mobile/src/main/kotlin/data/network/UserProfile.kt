package data.network

/**
 * Created by bogdan on 6/14/16.
 */
class UserProfile {
    var displayName: String? = null
    var photoUri: String? = null
}