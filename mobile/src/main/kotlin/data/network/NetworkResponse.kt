package data.network

/**
 * Created by bogdan on 6/10/16.
 */
interface NetworkResponse<T> {
    fun onSuccess(t: T)
    fun onFailure(message: String)
}