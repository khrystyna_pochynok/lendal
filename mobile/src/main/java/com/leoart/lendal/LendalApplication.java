package com.leoart.lendal;

import android.support.multidex.MultiDexApplication;

/**
 * Created by khrystyna on 6/9/16.
 */
public class LendalApplication extends MultiDexApplication {

    private static LendalApplication/**/ instance;

    public static LendalApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

}
