package com.leoart.lendal.utils

/**
 * Created by khrystyna on 6/22/16.
 */
class MessageBundl {

    val message: String?

    constructor(message: String?) {
        this.message = message
    }

    fun compose(): String {
        return message ?: "message was invalid to parse"
    }
}