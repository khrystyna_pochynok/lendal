package com.leoart.lendal.ui.auth.register

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.leoart.lendal.R
import com.leoart.lendal.ui.MainActivity
import com.leoart.lendal.ui.auth.AuthView
import com.pawegio.kandroid.find

/**
 * A login screen that offers login via email/password.
 */
class SignupActivity : AppCompatActivity(), AuthView {

    // UI references.
    var mEmailView : AutoCompleteTextView ? = null
    var mPasswordView : EditText ? = null
    var mProgressView : View ? = null

    var tvLogin : View ? = null
    var tvSignup : TextView ?  = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        initUI()
        initUIListeners()
    }

    private fun initUI() {
        mEmailView = find<AutoCompleteTextView>(R.id.email)
        mPasswordView = find<EditText>(R.id.password)
        mProgressView = find<View>(R.id.login_progress)
        tvLogin = find<TextView>(R.id.tvLogin)
        tvSignup = find<TextView>(R.id.tvSignup)
    }

    private fun initUIListeners() {
        tvLogin?.setOnClickListener({ goToLoginScreen() })
        tvSignup?.setOnClickListener({ attemptSignUp() })
        mPasswordView?.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptSignUp()
                return@OnEditorActionListener true
            }
            false
        })
    }

    internal fun goToLoginScreen() {
        finish()
    }

    internal fun attemptSignUp() {

        // Reset errors.
        mEmailView?.error = null
        mPasswordView?.error = null

        val email = mEmailView?.text.toString()
        val password = mPasswordView?.text.toString()

        var cancel = false
        var focusView: View? = null

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView?.error = getString(
                    R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView?.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView?.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()
        } else {
            showProgress(true)
            val presenter = SignUpPresenter(baseContext, this)
            presenter.register(email, password)
        }
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }

    override fun showProgress(show: Boolean) {
        mProgressView?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun goToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}

