package com.leoart.lendal.ui.auth.login

import android.content.Context
import android.widget.Toast

import com.leoart.lendal.ui.auth.AuthView
import com.pawegio.kandroid.e
import data.network.LoginRequest
import domain.staff.User
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by khrystyna on 6/13/16.
 */

class LoginPresenter(internal var view:

                     AuthView, internal var context: Context) {

    fun login(email: String, password: String) {
        LoginRequest(context)
                .login(email, password)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<User>() {
                    override fun onCompleted() {
                        println("onComplete")
                    }

                    override fun onError(e: Throwable) {
                        view.showProgress(false)
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onNext(user: User) {
                        println("onNExt")
                        view.showProgress(false)
                        view.goToMain()
                    }
                })
    }
}
