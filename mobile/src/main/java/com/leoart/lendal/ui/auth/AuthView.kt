package com.leoart.lendal.ui.auth

/**
 * Created by khrystyna on 6/13/16.
 */

interface AuthView {
    fun showProgress(show: Boolean)
    fun goToMain()
}
