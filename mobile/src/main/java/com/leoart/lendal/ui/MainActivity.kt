package com.leoart.lendal.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import com.leoart.lendal.R
import com.leoart.lendal.ui.auth.login.LoginActivity
import com.leoart.lendal.ui.my_staff_list.MyStaffListFragment
import com.leoart.lendal.ui.my_staff_list.MyStaffListFragment.OnListFragmentInteractionListener
import com.leoart.lendal.ui.my_staff_list.dummy.DummyContent
import com.pawegio.kandroid.find

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener, OnListFragmentInteractionListener {

    var flMainContainer : FrameLayout? = null
    var drawer : DrawerLayout? = null
    var toolbar : Toolbar? = null
    private var currentFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUI()
        setSupportActionBar(toolbar)
        initDrawer(toolbar)
        showMyStaffFragment(savedInstanceState)
    }

    private fun initUI() {
        flMainContainer = find<FrameLayout>(R.id.flMainContainer)
        drawer = find<DrawerLayout>(R.id.drawer_layout)
        toolbar = find<Toolbar>(R.id.toolbar)
    }

    private fun showMyStaffFragment(savedInstanceState: Bundle?) {
        setTitle(R.string.t_my_staff)
        if (savedInstanceState != null) {
            getCurrentFragmentFromSavedState(savedInstanceState)
        } else {
            showNewMyStaffFragment()
        }
    }

    private fun showNewMyStaffFragment() {
        if (supportFragmentManager.findFragmentByTag(MyStaffListFragment::class.java.simpleName) != null) {
            Log.d(TAG, "From fragment manager")
            currentFragment = supportFragmentManager.findFragmentByTag(MyStaffListFragment::class.java.simpleName)
        } else {
            currentFragment = MyStaffListFragment.newInstance()
        }
        //        currentFragment.setArguments(getIntent().getExtras());
        supportFragmentManager.beginTransaction().replace(R.id.flMainContainer, currentFragment,
                MyStaffListFragment::class.java.simpleName).commit()
    }

    private fun getCurrentFragmentFromSavedState(savedInstanceState: Bundle) {
        currentFragment = supportFragmentManager.getFragment(savedInstanceState, CURR_FRAGMENT)
        if (currentFragment != null) {
            supportFragmentManager.beginTransaction().replace(R.id.flMainContainer, currentFragment,
                    MyStaffListFragment::class.java.simpleName).commit()
        } else {
            showNewMyStaffFragment()
        }
    }

    private fun initDrawer(toolbar: Toolbar?) {
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        drawer?.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.nav_view) as NavigationView?
        navigationView?.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
//        if (drawer!=null &&  drawer.isDrawerOpen(find<NavigationView>(R.id.nav_view))) {
//            drawer?.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    @SuppressWarnings("StatementWithEmptyBody")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        Log.d(TAG, item.toString())
        val id = item.itemId
        when (id) {
            R.id.nav_my_staffa -> {
            }
            R.id.nav_friends_staff -> {
                showFriendsStaffFragment()
            }
            R.id.nav_share -> {
                shareApp()
            }
            R.id.nav_rate_us -> {
                rateApp()
            }
            R.id.nav_logout -> {
                logout()
            }
        }
        drawer?.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showFriendsStaffFragment() {

    }

    private fun shareApp() {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/html"
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>Hey, Check out this awesome app</p>"))
        startActivity(Intent.createChooser(sharingIntent, "Share using"))
    }

    private fun rateApp() {
        val appPackageName = packageName // getPackageName() from Context or Activity object
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
        } catch (anfe: android.content.ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)))
        }

    }

    private fun logout() {
        // TODO clear data
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    override fun onListFragmentInteraction(item: DummyContent.DummyItem) {
        // TODO
    }

    companion object {

        private val TAG = MainActivity::class.java.simpleName
        private val CURR_FRAGMENT = "current_fragment"
    }
}
