package com.leoart.lendal.ui.auth.login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.leoart.lendal.R
import com.leoart.lendal.ui.MainActivity
import com.leoart.lendal.ui.auth.AuthView
import com.leoart.lendal.ui.auth.register.SignupActivity
import com.pawegio.kandroid.find
import com.pawegio.kandroid.startActivity

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(), AuthView {

    // UI references
    private var mEmailView : AutoCompleteTextView? = null
    private var mPasswordView : EditText? = null
    private var mProgressView : View? = null
    private var tvSignUp : TextView? = null
    private var tvLogin : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initUI()
        attachUIListeners()
    }

    private fun initUI() {
        mEmailView = find<AutoCompleteTextView>(R.id.email)
        mPasswordView = find<EditText>(R.id.password)
        mProgressView = find<View>(R.id.login_progress)
        tvSignUp = find<TextView>(R.id.tvSignup)
        tvLogin = find<TextView>(R.id.tvLogin)
    }

    fun attachUIListeners() {
        tvSignUp?.setOnClickListener({ goToRegisterScreen() })
        tvLogin?.setOnClickListener({ attemptLogin() })
        mPasswordView?.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })
    }

    internal fun goToRegisterScreen() {
        startActivity<SignupActivity>()
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    internal fun attemptLogin() {
        // Reset errors.
        mEmailView?.error = null
        mPasswordView?.error = null

        val email = mEmailView?.text.toString()
        val password = mPasswordView?.text.toString()

        var cancel = false
        var focusView: View? = null

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView?.error = getString(
                    R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView?.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView?.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            focusView?.requestFocus()
        } else {
            showProgress(true)
            val presenter = LoginPresenter(this, baseContext)
            presenter.login(email, password)
            //register(email, password);
        }
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }

    override fun showProgress(show: Boolean) {
        mProgressView?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun goToMain() {
        startActivity<MainActivity>();
    }

}

