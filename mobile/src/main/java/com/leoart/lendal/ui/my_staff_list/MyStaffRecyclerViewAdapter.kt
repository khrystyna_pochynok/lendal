package com.leoart.lendal.ui.my_staff_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pawegio.kandroid.find

import com.leoart.lendal.R
import com.leoart.lendal.ui.my_staff_list.dummy.DummyContent.DummyItem


import com.leoart.lendal.ui.my_staff_list.MyStaffListFragment.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class MyStaffRecyclerViewAdapter(private val mValues: List<DummyItem>,
                                 private val mListener: OnListFragmentInteractionListener?) :
        RecyclerView.Adapter<MyStaffRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_my_staff_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.tvStaffName.text = mValues[position].id
        holder.tvStaffName.text = mValues[position].content

        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem as DummyItem)
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        var mItem: DummyItem? = null

        val ivItemPicture = mView.find<ImageView>(R.id.ivItemPicture)
        val  tvStaffName = mView.find<TextView>(R.id.tvStaffName)

        override fun toString(): String {
            return super.toString() + " '" + tvStaffName.text + "'"
        }
    }
}
