package com.leoart.lendal.ui.auth.register

import android.content.Context
import android.widget.Toast
import com.leoart.lendal.utils.MessageBundl

import com.leoart.lendal.ui.auth.AuthView
import com.pawegio.kandroid.v
import data.network.RegistrationRequest
import domain.staff.User

import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by khrystyna on 6/13/16.
 */

class SignUpPresenter(internal var context:
                      Context, internal var authView: AuthView) {

    fun register(email: String, password: String) {
        authView.showProgress(true)
        RegistrationRequest(context)
                .register(email, password)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<User>() {
                    override fun onCompleted() {
                        authView.showProgress(false)
                        System.out.println("onComplete")
                    }

                    override fun onError(e: Throwable) {
                        authView.showProgress(false)
                        v ("TAG" , MessageBundl(e.message).compose())
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onNext(user: User) {
                        authView.showProgress(false)
                        authView.goToMain()
                        System.out.println("onNExt")
                    }
                })
    }
}
