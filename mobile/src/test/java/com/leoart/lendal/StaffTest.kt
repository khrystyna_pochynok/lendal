package com.leoart.lendal

import data.DemoDataProvider
import domain.staff.StaffEntity
import domain.staff.User
import domain.staff.UserStaffCollection
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
class StaffTest {

    private var user: User? = null
    private var staffsList: MutableList<StaffEntity>? = null
    private var staffCollection: UserStaffCollection? = null

    @Before
    fun setup() {
        user = User("email@gmail.com", "password")
        staffsList = DemoDataProvider().getStaffList()
        user?.staffs = ArrayList<StaffEntity>()
        user?.staffs?.addAll(staffsList!!)
        staffCollection = UserStaffCollection()
    }

    @Test
    @Throws(Exception::class)
    fun testAddingStaff() {
        assertEquals(user?.staffs?.size, staffsList?.size)
    }

    @Test
    @Throws(Exception::class)
    fun testRemovingStaff(){
        val size = user?.staffs?.size ?: 0
        user?.staffs?.removeAt(0)
        assertEquals(user?.staffs?.size ?: 0, size - 1)
    }
}